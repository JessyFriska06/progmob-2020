package ukdw.example.jessyprogmob.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.example.jessyprogmob.Adapter.MahasiswaRecyclerAdapter;
import ukdw.example.jessyprogmob.Model.Mahasiswa;
import ukdw.example.jessyprogmob.R;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        RecyclerView rv = (RecyclerView)findViewById(R.id.RvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //datadummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Jessy", "72180248", "081522736263");
        Mahasiswa m2 = new Mahasiswa("Maytha", "72180238", "081522736264");
        Mahasiswa m3 = new Mahasiswa("Nova", "72180236", "081522736265");
        Mahasiswa m4 = new Mahasiswa("Boy", "71180277", "081522736266");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);


    }
}